Command line instructions

You can also upload existing files from your computer using the instructions below.

Git global setup

git config --global user.name "Saúl Parra"
git config --global user.email "saulperazaparra@gmail.com"

git init para indicar el contexto de trabajo (la carpeta en la que se va a trabajar pues)


Create a new repository

git clone https://gitlab.com/AlterParra/proyecto-definitivo.git
git pull para actualizar el repositorio local
cd proyecto-definitivo
git switch -c main
touch README.md
git add .
git commit -m "comentario"
git push -u origin main

Push an existing folder

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/AlterParra/proyecto-definitivo.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/AlterParra/proyecto-definitivo.git
git push -u origin --all
git push -u origin --tags
